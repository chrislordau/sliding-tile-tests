#!/bin/sh
for f in tests/*.in
do
    f=`echo $f | sed -E 's/tests\/([0-9a-z\-]+).in/\1/'`
    ./puzzle < "tests/$f.in" > "tests/$f.out"
    echo "Test \"$f\""
    diff "tests/$f.out" "tests/$f.exp"
done
